---
layout: post
title:  "Waclae"
date:   2017-08-13
categories: jekyll update
---
Wacław Kostek-Biernacki (1882–1957) was a Polish interwar politician and a popular fantasy writer (pen name Brunon Kostecki)[1] as well as a Polish soldier of World War II, imprisoned and blacklisted in Stalinist Poland. In his youth, he was an activist in the Polish Socialist Party, and member of the secret Polish Military Organisation during World War I. Kostek-Biernacki joined the Polish Legions in World War I under Józef Piłsudski. He supported the May Coup d'État of 1926.[2]

He was a Voivode of Nowogródek Voivodeship from 1931 to 1932, and of Polesie Voivodeship from 1932 to 1939. Only in his capacity of a voivode, he supervised the operation of nearby Bereza Kartuska; nonetheless, he also took a lot of interest in it, often to the detriment of communist prisoners whose sentences were sometimes prolonged extra-judicially.[3]

Also in 1932, Kostek-Biernacki published his best-known collection of horror stories and novellas called Straszny gość (Ghastly guest) under the pen name Brunon Kostecki. The book featured six titles: "Twarda proswirka", "Straszny gość", "Zdradliwe żonki", "Zmora", "Kamienne krzyże", and "Chytrość Marusi". After the German invasion of Poland in 1939 he left Poland with the evacuating government. He was interned in Romania until 1944, and deported back to Poland by Romanian communists in 1945. He was arrested by the Communist secret police Urząd Bezpieczeństwa and after eight years without trial spent in Mokotów Prison, he was finally condemned to death in April 1953 on fake charges of "supporting fascism." His sentence was commuted to 10 years in prison. His books were deliberately destroyed. Biernacki was released after amnesty of 1955, and died in 1957.[2]