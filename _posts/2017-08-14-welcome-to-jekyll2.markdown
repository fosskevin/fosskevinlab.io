---
layout: post
title:  "Drone"
categories: jekyll update
---
Musical effect

"Of all harmonic devices, it [a drone] is not only the simplest, but probably also the most fertile".[6]

A drone effect can be achieved through a sustained sound or through repetition of a note. It most often establishes a tonality upon which the rest of the piece is built. A drone can be instrumental, vocal or both. Drone (both instrumental and vocal) can be placed in different ranges of the polyphonic texture: in the lowest part, in the highest part, or in the middle. The drone is most often placed upon the tonic or dominant (play "Row, Row, Row Your Boat" with a drone on the About this sound tonic (help·info), on the About this sound dominant (help·info), or on About this sound both (help·info). Compare with About this sound changing chords (help·info).). A drone on the same pitch as a melodic note tends to both hide that note and to bring attention to it by increasing its importance.

A drone differs from a pedal tone or point in degree or quality. A pedal point may be a form of nonchord tone and thus required to resolve unlike a drone, or a pedal point may simply be considered a shorter drone, a drone being a longer pedal point.
History and distribution
A Lady Playing the Tanpura, ca. 1735.

The systematic use of drones originated in instrumental music of ancient Southwest Asia, and spread north and west to Europe, east to India, and south to Africa.[7] It is a key component of much Australian aboriginal music through the didgeridoo. It is used in Indian music and is played with the tanpura (or tambura) and other Indian drone instruments like the ottu, the ektar, the dotara (or dotar; dutar in Persian Central Asia), the surpeti, the surmandal (or swarmandal) and the shankh (conch shell). Most of the types of bagpipes that exist worldwide have up to three drones, making this one of the first instruments that comes to mind when speaking of drone music. In America, most forms of the African-influenced banjo contain a drone string. Since the 1960s, the drone has become a prominent feature in drone music and other forms of avant-garde music.

In vocal music drone is particularly widespread in traditional musical cultures, particularly in Europe, Polynesia and Melanesia. "Drones are not uncommon in primitive music, but neither are they characteristic of it".[8] It is also present in some isolated regions of Asia (like among Pearl-divers in the Persian Gulf, some national minorities of South-West China, Taiwan, Vietnam, and Afghanistan).[9][page needed]
Part(s) of a musical instrument
Highland bagpipes, with drone pipes over the pipers' left shoulders

Drone is also the term for the part of a musical instrument intended to produce the drone effect's sustained pitch, generally without the ongoing attention of the player. Different melodic Indian instruments (e.g. the sitar, the sarod, the sarangi and the rudra veena) contain a drone. For example, the sitar features three or four resonating drone strings, and Indian notes (sargam) are practiced to a drone. Bagpipes (like the Great Highland Bagpipe and the Zampogna) feature a number of drone pipes, giving the instruments their characteristic sounds. A hurdy-gurdy has one or more drone strings. The fifth string on a five-string banjo is a drone string with a separate tuning peg that places the end of the string five frets down the neck of the instrument; this string is usually tuned to the same note as that which the first string produces when played at the fifth fret, and the drone string is seldom fretted. The bass strings of the Slovenian drone zither also freely resonate as a drone. The Welsh Crwth also features two drone strings.
Melody to "Yankee Doodle" without and with drone notes as played on the banjo[10] About this sound Play without (help·info) and About this sound with drone (help·info).
Use in musical compositions

Composers of Western classical music occasionally used a drone (especially one on open fifths) to evoke a rustic or archaic atmosphere, perhaps echoing that of Scottish or other early or folk music. Examples include the following:

    Haydn, Symphony No. 104, "London", opening of finale, accompanying a folk melody[citation needed]
    Beethoven, Symphony No. 6, "Pastoral", opening and trio section of scherzo[citation needed]
    Felix Mendelssohn, Symphony No. 3 in A minor, opus 56, 'Scottish', especially the finale.
    Frederic Chopin, Mazurkas,_Op._7_(Chopin), all five mazurkas contain a drone
    Berlioz, Harold in Italy, accompanying oboes as they imitate the piffero of Italian peasants[citation needed]
    Richard Strauss, Also sprach Zarathustra, Introduction: the opening grows out of a drone effect in the orchestra.
    Mahler, Symphony No. 1, introduction; a seven-octave drone on A evokes "the awakening of nature at the earliest dawn"[citation needed]
    Bartók, in his adaptations for piano of Hungarian and other folk music[citation needed]

The best-known drone piece in the concert repertory is the Prelude to Wagner's Das Rheingold (1854) wherein low horns and bass instruments sustain an E♭ throughout the entire movement.[11] The atmospheric ostinato effect that opens Beethoven's Ninth Symphony, which inspired similar gestures in the opening of all the symphonies of Anton Bruckner, represents a gesture derivative of drones.

One consideration for composers of common practice keyboard music was equal temperament. The adjustments lead to slight mistunings as heard against a sustained drone. Even so, drones have often been used to spotlight dissonance purposefully.

Modern concert musicians make frequent use of drones, often with just or other non-equal tempered tunings. Drones are a regular feature in the music of composers indebted to the chant tradition, such as Arvo Pärt, Sofia Gubaidulina, and John Tavener. The single-tones that provided the impetus for minimalism through the music of La Monte Young and many of his students qualify as drones. David First, the band Coil, the early experimental compilations of John Cale (Sun Blindness Music, Dream Interpretation, and Stainless Gamelan), Pauline Oliveros and Stuart Dempster, Alvin Lucier (Music On A Long Thin Wire), Ellen Fullman, Lawrence Chandler and Arnold Dreyblatt all make notable use of drones. The music of Italian composer Giacinto Scelsi is essentially drone-based. Shorter drones or the general concept of a continuous element are often used by many other composers. Other composers whose music is entirely based on drones include Charlemagne Palestine and Phill Niblock. Drone pieces also include Loren Rush's Hard Music (1970) [12] and Folke Rabe's Was?? (1968),[13] as well as Robert Erickson's Down at Piraeus.[14] The avant-garde guitarist Glenn Branca also uses drones extensively.

Drones continue to be characteristic of folk music. Early songs by Bob Dylan employ the effect with a retuned guitar in "Masters of War" and "Mr. Tambourine Man".[citation needed] The song "You Will Be My Ain True Love", written by Sting for the 2003 movie Cold Mountain and performed by Alison Krauss and Sting, uses drone bass.[citation needed]

Drones are used widely in the blues and blues-derived genres. Jerry Lee Lewis featured drones in solos and fills.[15] Drones were virtually absent in original rock and roll music,[citation needed] but gained popularity after the Beatles used drones in a few popular compositions (for example, "Blackbird" has a drone in the middle of a texture throughout the whole song, "Tomorrow Never Knows" makes use of tambura). They also used high drone for the dramatic effect in some sections of several of their compositions (like the last verses of "Yesterday" and "Eleanor Rigby").[citation needed] The rock band U2 uses drones in their compositions particularly widely.[citation needed] In the Led Zeppelin song "In The Light", a keyboard drone is used throughout the song, mostly in the intro.[citation needed]
Use for musical training
See also: Ear training

Drones are used by a number of music education programs for ear training and pitch awareness, as well as a way to improvise ensemble music.[16] A shruti box is often used by vocalists in this style of musical training. Drones, owing to their acoustic properties and following their longstanding use in ritual and chant, can be useful in constructing aural structures outside common practice expectations of harmony and melody.[17][verification needed]
See also

    Drone metal - a form of heavy metal music focusing almost entirely on droning, heavily downtuned electric guitar and bass guitar, often lacking vocals or drums.
    Jivari
